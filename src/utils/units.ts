import { config } from "../config";

export type TimeUnit = number|string;

const units: Record<string, (v: number) => number> = {
	s: v => v * config.fps,
	m: v => v * 60 * config.fps,
	f: v => v,
}

export function unitsToFrames(value: TimeUnit) {
	let unit = /[a-z]$/.exec("" + value)?.[0];
	let number = +("" + value).slice(0, -(unit?.length || 0));
	unit ||= "s";

	if (!units[unit]) throw new Error(`Invalid time unit for time value '${number}' '${unit}' - available units are ${Object.keys(units)}`);

	return units[unit](number);
}
