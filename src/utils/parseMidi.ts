import midi from 'midi-file';
import { MidiEvent } from "../hooks";

function basename(str: string) {
	return str.split("/").at(-1)?.split(".")[0];
}

export async function parseMidi(path: string, renderFramesPerSecond: number) {
	const events: MidiEvent[] = [];

	const buffer = await fetch(path).then(r => r.arrayBuffer());
	const { header, tracks } = midi.parseMidi(new Uint8Array(buffer));
	console.log("midi:", header);

	// default beats per second = 2 (120 BPM)
	let beatsPerSecond = 2;
	let beatsPerSecondDefault = true;

	// if constant ticksPerFrame specified, use that - otherwise, rely on beatsPerSecond
	const ticksPerSecond = () => header.ticksPerFrame && header.framesPerSecond
		? header.ticksPerFrame * header.framesPerSecond
		: (header.ticksPerBeat || 256) * beatsPerSecond;

	const ticksPerRenderFrame = () => ticksPerSecond() / renderFramesPerSecond;

	let lengthTicks = 0;

	tracks.forEach((track, index) => {
		// tick starts at 0 on each track
		let tick = 0;

		for (const event of track) {
			// add the deltaTime of each event processed
			tick += event.deltaTime;

			if (event.type === "setTempo" && beatsPerSecondDefault) {
				beatsPerSecond = 1 / (event.microsecondsPerBeat * 1e-6);
				beatsPerSecondDefault = false;
			}

			if (event.type === "noteOn") {
				events.push({
					name: `${basename(path)}${index}`,
					frame: Math.round(tick / ticksPerRenderFrame()),
					value: event.noteNumber,
					intensity: event.velocity,
				});
			}
		}

		// measure the last event in the file
		if (tick > lengthTicks)
			lengthTicks = tick;
	});

	return {
		events,
		lengthTicks,
		lengthFrames: Math.round(lengthTicks / ticksPerRenderFrame()),
	};
}

export async function parseMidis(paths: string[], fps: number) {
	return Promise.all(
		paths.map((path) => parseMidi(path, fps))
	);
}
