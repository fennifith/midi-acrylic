import { MidiTrack } from './hooks';
import { TimeUnit } from './utils/units';

// TODO: main.ts doesn't seem like the right place for this...
export type Config = {
	fps: number;
	tracks: MidiTrack[];
};

export const config: Config = {
	fps: 60,
	tracks: [{
		midi: ['./glass temper.mid'],
		audio: './glass temper.flac',
		audioStart: "7.2s",
	},{
		midi: ['./acrylic.mid'],
		audio: './acrylic.flac',
		audioStart: "7.2s",
	},{
		midi: ['./outrush.mid'],
		padEnd: "10s",
		audio: './outrush.flac',
		audioStart: "12.85s",
	},{
		midi: ['./deluge.mid'],
		audio: './deluge.flac',
		audioStart: "6.35s",
	},{
		midi: ['./unease.mid'],
		audio: './unease.flac',
		audioStart: "4.2s",
	}],
};
