import { onScopeDispose } from "vue";
import { MidiEvent } from "./createMidiScheduler";
import { useMidiScheduler } from "./useMidiScheduler";

export function onMidiEvent(name: string, callback: (event: MidiEvent) => void) {
	const scheduler = useMidiScheduler();
	scheduler.addEventListener(name, callback);

	onScopeDispose(() => {
		scheduler.removeEventListener(callback);
	});
}
