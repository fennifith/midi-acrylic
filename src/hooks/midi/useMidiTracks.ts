import { ref } from "vue";
import { onBeforeRender } from "vue-three-js";
import { config } from "../../config";
import { parseMidi, parseMidis } from "../../utils/parseMidi";
import { TimeUnit, unitsToFrames } from "../../utils/units";
import { createMidiScheduler } from "./createMidiScheduler";

export type MidiTrack = {
	midi: string[];
	padStart?: TimeUnit;
	padEnd?: TimeUnit;
	audio: string;
	audioStart?: TimeUnit;
};

type MidiTrackProcessed = {
	midi: Awaited<ReturnType<typeof parseMidi>>[];
	midiFrames: number;
	padStart: number;
	padEnd: number;
	audio: HTMLAudioElement;
	audioStart: number;
};

export async function useMidiTracks(inputTracks: MidiTrack[]) {
	const scheduler = createMidiScheduler();

	let isPlaying = ref(false);
	let trackIndex = 0;
	let frame = 0;

	const tracks: MidiTrackProcessed[] = await Promise.all(inputTracks.map(async (track) => {
		const midi = await parseMidis(track.midi, config.fps);
		const padStart = unitsToFrames(track.padStart || 0);
		const padEnd = unitsToFrames(track.padEnd || 0);
		const midiFrames = padStart + Math.max(...midi.map(m => m.lengthFrames)) + padEnd;

		return {
			midi,
			padStart,
			padEnd,
			midiFrames,
			audio: new Audio(track.audio),
			audioStart: padStart + unitsToFrames(track.audioStart || 0),
		};
	}));

	function play() {
		playTrack(tracks[0]);
	}

	function playTrack(track: MidiTrackProcessed) {
		if (isPlaying.value) return;

		// schedule all events
		scheduler.clear();
		for (const midi of track.midi) {
			for (const event of midi.events) {
				scheduler.schedule({
					...event,
					frame: track.padStart + event.frame,
				});
			}
		}

		console.log(`playing track ${trackIndex} for ${track.midiFrames} frames`);

		isPlaying.value = true;
		frame = 0;
	}

	function dispatchFrame() {
		if (!isPlaying.value) return;

		const track = tracks[trackIndex];
		if (track.audio && track.audioStart === frame) {
			track.audio.play();
		}

		scheduler.dispatchFrame(frame++);

		if (frame > tracks[trackIndex].midiFrames) {
			console.log("track over");
			isPlaying.value = false;

			if (trackIndex < inputTracks.length - 1) {
				trackIndex += 1;
				playTrack(tracks[trackIndex]);
			}
		}
	}

	return { isPlaying, play, dispatchFrame };
}
