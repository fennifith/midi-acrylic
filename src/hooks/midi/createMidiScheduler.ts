import { InjectionKey, provide } from "vue";

export type MidiEvent = {
	name: string;
	frame: number;
	value: number;
	intensity: number;
};

export type MidiEventListener = {
	name: string;
	callback: (e: MidiEvent) => void | Promise<void>;
	offset?: number;
};

export function createMidiScheduler() {
	let events: Record<number, MidiEvent[]> = {};
	let listeners: MidiEventListener[] = [];

	function schedule(event: MidiEvent) {
		events[event.frame] ??= [];
		events[event.frame].push(event);
	}

	async function dispatchEvent(event: MidiEvent) {
		await Promise.all(
			listeners
				.filter(listener => event.name === listener.name)
				.map(listener => listener.callback(event))
		);
	}

	async function dispatchFrame(frame: number) {
		await Promise.all(listeners.flatMap(listener => {
			// find events for current frame + listener offset
			const frameEvents = events[frame + (listener.offset || 0)];
			if (!frameEvents) return [];

			// run callback for each event
			return frameEvents
				.filter(e => e.name === listener.name)
				.map(listener.callback);
		}));
	}

	function addEventListener(name: string, callback: MidiEventListener["callback"], offset?: number) {
		listeners.push({ name, callback, offset });

		return {
			remove() { removeEventListener(callback); }
		};
	}

	function removeEventListener(callback: MidiEventListener["callback"]) {
		listeners = listeners.filter((l) => l.callback !== callback);
	}

	function clear() {
		events = {};
	}

	const scheduler = {
		schedule, dispatchEvent, dispatchFrame,
		addEventListener, removeEventListener, clear,
	};

	provide(EventSchedulerSymbol, scheduler);
	return scheduler;
}

export type EventScheduler = ReturnType<typeof createMidiScheduler>;

export const EventSchedulerSymbol: InjectionKey<EventScheduler> = Symbol();
