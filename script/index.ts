import puppeteer from "puppeteer";
import * as fs from "fs";
import * as path from "path";
import ffmpeg from "fluent-ffmpeg";
import { Stream } from "stream";
import { config } from "../src/config";
import { exec } from "child_process";

// Start hosting the dist dir on an http server
const httpServer = exec("bash -c 'cd dist && pnpx http-server'");

// Capture the output URL when the server starts
const url = await new Promise(res => {
	httpServer.stdout?.on("data", data => {
		const match = /http\:[^\s]+/g.exec(data.toString());
		if (match && match[0]) {
			res(match[0]);
		}
	});
});

const browser = await puppeteer.launch({
	//headless: false,
	args: [`--window-size=1920,1080`, '--use-gl=egl'],
	defaultViewport: {
		width: 1920,
		height: 1080,
	},
	dumpio: true,
});
const page = await browser.newPage();
//await page.goto("file://" + path.resolve("dist/index.html") + "?autoplay=false", { waitUntil: "networkidle0" });
await page.goto(url + "?script=true");

async function* renderer() {
	for (let i = 0; true; i++) {
		if (i % 100 === 0) console.log(`frame ${i}`);
		const ret = await page.evaluate(`window.drawFrame()`);
		yield (await page.screenshot());

		if (!ret) break;
	}
}

const stream = Stream.Readable.from(renderer());

console.log("starting ffmpeg:");

ffmpeg(stream)
	.inputFormat("image2pipe")
	.inputFps(config.fps)
	.inputOptions(
		"-video_size", `1920x1080`,
		"-pixel_format", "yuv420p",
	)
	.output("output.mp4")
	.run();

stream.on("close", () => {
	browser.close();
	httpServer.kill();
});
